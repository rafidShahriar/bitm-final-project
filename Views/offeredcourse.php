<?php

include_once '../vendor/autoload.php';

use App\course\Course;

$objIndex = new Course();

$allData = $objIndex->ShowAllCourse();
$i = 1;
$serial = 1;
?>

<?php include_once 'header.php'; ?>
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">List</span> Of All Courses</h4>
        </div><?php $objIndex->sessionMsg('deleteSuccess') ?>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Detached sidebar -->
    <?php include_once 'sidebar.php'; ?>
    <!-- /detached sidebar -->


    <!-- Detached content -->
    <div class="container-detached">
        <div class="content-detached">

            <!-- Grid -->
            <div class="row">
                <div class="col-md-12">

                    <!-- Horizontal form -->
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-columned footable" data-page-size="10"
                                       data-next-text="Next" data-previous-text="Previous">
                                    <thead>
                                        <tr class="disabled bg-teal-600">
                                            <th>#</th>
                                            <th>Course<br>Name</th>
                                            <th>Duration</th>
                                            <th>Type</th>
                                            <th>Course<br>Price</th>
                                            <th colspan="3"><center>Action</center></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($allData as $singleData) {
                                            if($singleData['is_offer']==1){
                                                 continue;
                                            
                                            }
                                            if ($singleData['is_delete'] == 1) {
                                                continue;
                                            }
                                            ?>
                                            <tr class="<?php
                                            if ($i % 2 == 0) {
                                                echo "info";
                                            } else {
                                                echo "alpha-slate";
                                            }
                                            $i++;
                                            ?>">
                                                <td><?php echo $serial++; ?></td>
                                                <td><?php echo $singleData['title']; ?></td>
                                                <td><?php
                                                    if ($singleData['duration'] == '15_days') {
                                                        echo "15 Days Long";
                                                    }
                                                    if ($singleData['duration'] == '1_month') {
                                                        echo "1 Month Long";
                                                    }
                                                    if ($singleData['duration'] == '2_months') {
                                                        echo "2 Months Long";
                                                    }
                                                    if ($singleData['duration'] == '3_months') {
                                                        echo "Maximum 3 Months";
                                                    }
                                                    ?></td>
                                          <?php
                                                if ($singleData['course_type'] == 2) {
                                                    echo '<td><b><span class="text-danger">FREE Course</span></b></td>';
                                                } else {
                                                    echo '<td>Paid Course</td>';
                                                }
                                                ?>
                                               <td>
                                                    <?php
                                                    if ($singleData['course_fee'] == 0) {
                                                        echo'Not Applicable';
                                                    } else {
                                                        echo $singleData['course_fee'];
                                                    }
                                                    ?>
                                                    <?php
                                                    if ($singleData['is_offer'] == 0) {
                                                        echo '<br><span class="badge bg-danger-400">Offered</span>';
                                                    }
                                                    ?>
                                                </td>
                                                
                                                <td class="text-center">
                                                    <a class="btn bg-teal" type="button" href="show.php?id=<?php echo $singleData['unique_id']; ?>">
                                                        <i class="icon-enlarge6 position-left"></i>
                                                        Details
                                                    </a>
                                                </td>    
                                                    
                                                <td class="text-center">
                                                    <a class="btn bg-teal" type="button" href="edit.php?id=<?php echo $singleData['unique_id']; ?>">
                                                        <i class="icon-pencil7 position-left"></i>
                                                        Edit
                                                    </a>
                                                </td>
                                                
                                                <td class="text-center">
                                                    <a class="btn bg-teal" type="button" href="trash.php?id=<?php echo $singleData['unique_id']; ?>">
                                                        <i class="icon-close2 position-left"></i>
                                                        Delete
                                                    </a>
                                                </td>
                                            </tr>

                                            <?php
                                        }
                                        
                                        ?>
                                    </tbody>

                                    <!-- Pagination -->
                                    <tfoot class="hide-if-no-paging">
                                        <tr>
                                            <td colspan="3">
                                                <div class="pagination text-center"></div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                    <!-- /Pagination -->

                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /horizotal form -->

                </div>

            </div>
        </div>
        <!-- /grid -->

    </div>
</div>
<!-- /detached content -->
<?php include_once 'footer.php';?>





