<?php
include_once "../../vendor/autoload.php";

use App\assign\Assign;

$obj = new Assign();

$allTrainers = $obj->AllTrainers();
//
$allLabs = $obj->AllLabs();

$allDatas = $obj->allData();

$allData = $obj->ShowAllAssign();
//echo"<pre>";
//print_r($allData);

if (!empty($_SESSION['EditPost']) && isset($_SESSION['EditPost'])) {
    session_unset($_SESSION['EditPost']);
}


$i = 1; $p = 1;
$serial = 1;

?>

<?php include_once 'header.php'; ?>
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">List</span> Of All Courses</h4>
        </div><?php $obj->sessionMsg('deleteSuccess') ?>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Detached sidebar -->
    <?php include_once 'sidebar.php'; ?>
    <!-- /detached sidebar -->


    <!-- Detached content -->
    <div class="container-detached">
        <div class="content-detached">

            <!-- Grid -->
            <div class="row">
                <div class="col-md-12">

                    <!-- Horizontal form -->
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table footable" data-page-size="10"
                                       data-next-text="Next" data-previous-text="Previous">
                                    <thead>
                                        <tr class="disabled bg-teal-600">
                                            <th>#</th>
                                            <th>Course<br>Name</th>
                                            <th>Batch_no</th>
                                            <th>Lead<br>Trainer</th>
                                            <th>Assist<br>Trainer</th>
                                            <th>Lab<br>Assist</th>
                                            <th>Lab<br>Num</th>
                                            <th>Start<br>Date</th>
                                            <th>End<br>Date</th>
                                            <th>Day</th>
                                            <th>Assigned By</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($allData as $singleData) {
                                            if ($singleData['is_running'] == 0) {
                                                continue;
                                            }
                                            ?>
                                            <tr class="<?php
                                            if ($i % 2 == 0) {
                                                echo "info";
                                            } else {
                                                echo "alpha-slate";
                                            }
                                            $i++;
                                            ?>">
                                                <td><?php echo $serial++; ?></td>
                                                <td><?php echo $singleData['title']; ?></td>
                                                
                                                <td><?php echo $singleData['batch_no']; ?></td>
                                                <td><?php echo $singleData['lead_trainer']; ?></td>
                                                <td><?php echo $singleData['asst_trainer']; ?></td>
                                                <td><?php echo $singleData['lab_asst']; ?></td>
                                                <td><?php echo $singleData['lab_id']; ?></td>
                                                <td><?php echo $singleData['start_date']; ?></td>
                                                <td><?php echo $singleData['ending_date']; ?></td>
                                                <td><?php
                                                if($singleData['day']== 'day1'){
                                                echo 'Sat-Mon-Wed sesseion';}
                                                 if($singleData['day']== 'day2'){
                                                 echo 'Sun-Tue-Thu sesseion';}
                                                  if($singleData['day']== 'day3'){
                                                  echo 'Friday sesseion';}
                                                    ?></td>
                                                <td><?php echo $singleData['assigned_by']; ?></td>                                              
                                            </tr>
                                            <tr class="<?php
                                            if ($p % 2 == 0) {
                                                echo "info";
                                            } else {
                                                echo "alpha-slate";
                                            }
                                            $p++;
                                            ?>">
                                                <td class="text-center" colspan="11">
                                                    <a class="btn bg-teal-600" type="button" href="show.php?id=<?php echo $singleData['id']; ?>">
                                                        <i class="icon-enlarge6 position-left"></i>
                                                        Details
                                                    </a>
                                               
                                                    <a class="btn bg-teal-800" type="button" href="edit.php?id=<?php echo $singleData['id']; ?>">
                                                        <i class="icon-pencil7 position-left"></i>
                                                        Edit
                                                    </a>
                                                
                                                    <a href="trash.php?id=<?php echo $singleData['id']; ?>" class="btn bg-teal" onclick="return confirm('Are you sure you want to disable this course session?');"><i class="icon-close2 position-left"></i>
                                                        Disable
                                                    </a>
                                                </td>
                                            </tr>

                                            <?php
                                        }
                                        ?>
                                    </tbody>

                                    <!-- Pagination -->
                                    <tfoot class="hide-if-no-paging">
                                        <tr>
                                            <td colspan="3">
                                                <div class="pagination text-center"></div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                    <!-- /Pagination -->

                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /horizotal form -->
                    <div class="text-right">
                        <a class="btn bg-grey btn-labeled" type="button" href="trashlist.php"><b><i class="icon-truck"></i></b>Disabled Session List</a>
                    </div>

                </div>

            </div>
        </div>
        
</div> </div>
</div>
<!-- /detached content -->
<?php include_once 'footer.php';?>

