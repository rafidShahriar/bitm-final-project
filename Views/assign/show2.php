<?php
include_once "../../vendor/autoload.php";

use App\assign\Assign;

$obj = new Assign();

$obj->prepare($_GET);
$oneData = $obj->searchShow();

// print_r($oneData);
?>
<!-- /theme JS files -->

<?php include_once 'header.php'; ?>
<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header">
                <div class="page-header-content">
                    <div class="page-title">
                    <h4 class="text-center"><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Details of</span> <?php echo $oneData['title']; ?> Course</h4>
                    </div>
                </div>
            </div>
            <!-- /page header -->


            <!-- Content area -->
            <div class="content">

                <!-- detached sidebar -->
                <?php include_once 'sidebar.php'; ?>
                <!-- /sidebar -->

                <!-- deatched content -->
                <div class="container-detached">
                    <div class="content-detached">
                        
                    <!-- <div class="panel panel-flat">
                        something
                    </div> -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Horizontal form -->
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <h3 class="panel-title">Course Description </h3>
                            <div class="heading-elements">
                                <div class="heading-btn-group">
                                    <a type="button" class="btn bg-teal btn-labeled" href="edit.php?id=<?php echo $oneData['unique_id']; ?>"><b><i class="icon-pencil7"></i></b> Edit Course</a>
                                    
                                    <a type="button" onclick="return checkDelete()" class="btn bg-teal btn-labeled" href="trash.php?id=<?php echo $oneData['unique_id']; ?>"><b><i class="icon-close2"></i></b> Disable Course</a>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr class="success">
                                            <th class = "col-md-3"><b>ID</b></th>
                                            <td class = "col-md-9"><?php echo $oneData['id']; ?></td>
                                        </tr>
                                        <tr class="warning">
                                            <th class = "col-md-3"><b>Course Title</b></th>
                                            <td class = "col-md-9"><?php echo $oneData['title']; ?></td>
                                        </tr>
                                        <tr class="success">
                                            <th class = "col-md-3"><b>Batch No</b></th>
                                            <td class = "col-md-9"><?php echo $oneData['batch_no']; ?></td>
                                        </tr>
                                        <tr class="warning">
                                            <th class="col-md-3"><b>Lead Trainer</b></th>
                                            <td class="col-md-9"><?php echo $oneData['lead_trainer']; ?></td>
                                        </tr>
                                        <tr class="success">
                                            <th class = "col-md-3"><b>Asst. Trainer</b></th>
                                            <td class = "col-md-9"><?php echo $oneData['asst_trainer']; ?></td>
                                        </tr>
                                        <tr class="warning">
                                            <th class="col-md-3"><b>Lab Asst.</b></th>
                                            <td class="col-md-9"><?php echo $oneData['lab_asst']; ?></td>
                                        </tr>
                                        <tr class="success">
                                            <th class = "col-md-3"><b>Lab No</b></th>
                                            <td class = "col-md-9"><?php echo $oneData['lab_no']; ?></td>
                                        </tr>
                                        <tr class="warning">
                                            <th class="col-md-3"><b>Start Date</b></th>
                                            <td class="col-md-9"><?php echo $oneData['start_date']; ?></td>
                                        </tr>
                                        <tr class="success">
                                            <th class = "col-md-3"><b>Ending Date</b></th>
                                            <td class = "col-md-9"><?php echo $oneData['ending_date']; ?></td>
                                        </tr>
                                        <tr class="warning">
                                            <th class="col-md-3"><b>Start Time</b></th>
                                            <td class="col-md-9"><?php echo $oneData['start_time']; ?></td>
                                        </tr>
                                        <tr class="success">
                                            <th class = "col-md-3"><b>Ending Time</b></th>
                                            <td class = "col-md-9"><?php echo $oneData['ending_time']; ?></td>
                                        </tr>
                                        <tr class="warning">
                                            <th class="col-md-3"><b>Course Running</b></th>
                                            <td class="col-md-9">
                                                <?php
                                                if ($oneData['is_running'] == '1') {
                                                    echo "Yes";
                                                } else {
                                                    echo "No";
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr class="success">
                                            <th class = "col-md-3"><b>Assigned By</b></th>
                                            <td class = "col-md-9"><?php echo $oneData['assigned_by']; ?></td>
                                        </tr>
                                        <tr class="warning">
                                            <th class = "col-md-3"><b>Created</b></th>
                                            <td class = "col-md-9"><?php echo $oneData['created']; ?></td>
                                        </tr>
                                        <tr class="success">
                                            <th class = "col-md-3"><b>Modified</b></th>
                                            <td class = "col-md-9">
                                                <?php
                                                if ($oneData['updated'] == '0000-00-00 00:00:00') {
                                                    echo "No Modify Yet";
                                                } else {
                                                    echo $oneData['updated'];
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr class="warning">
                                            <th class = "col-md-3"><b>Delete</b></th>
                                            <td class = "col-md-9">
                                                <?php
                                                if ($oneData['deleted'] == '0000-00-00 00:00:00') {
                                                    echo "No Delete Yet";
                                                } else {
                                                    echo $oneData['deleted'];
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        </div>
                        <!-- close of Horizontal form -->
                    </div>
                
                    </div>
                </div>
                <script>
                    $(function () {
                        var conditionalSelect = $("#secondSelect"),
                                // Save possible options
                                options = conditionalSelect.children(".conditional").clone();

                        $("#firstSelect").change(function () {
                            var value = $(this).val();
                            conditionalSelect.children(".conditional").remove();
                            options.clone().filter("." + value).appendTo(conditionalSelect);
                        }).trigger("change");
                    });
                </script>

                <?php include_once 'footer.php'; ?>
