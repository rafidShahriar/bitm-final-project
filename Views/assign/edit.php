<?php
include_once "../../vendor/autoload.php";

use App\assign\Assign;

$obj = new Assign();

//$allCourse = $obj->ShowAllCourse();
//
$allTrainers = $obj->AllTrainers();
//
$allLabs = $obj->AllLabs();

$allDatas = $obj->allData();

$editData = $obj ->prepare($_GET)->assignShow();


?>
<!-- /theme JS files -->

<?php include_once 'header.php'; ?>
<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4 class="text-center"><i class="icon-arrow-right14 position-left"></i> <span class="text-semibold">Assign</span> New Course</h4>
                    </div><?php $obj->sessionMsg('assignStoreSuccess');
$obj->sessionMsg('deleteSuccess'); $obj->sessionMsg('assignUpdateSuccess');?>
                </div>
            </div>
            <!-- /page header -->


            <!-- Content area -->
            <div class="content">

                <!-- detached sidebar -->
                <?php include_once 'sidebar.php'; ?>
                <!-- /sidebar -->

                <!-- deatched content -->
                <div class="container-detached">
                    <div class="content-detached">
                        <form class="form-horizontal" action="update.php" method="POST">
                            <div class="panel panel-flat">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <fieldset>
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label">Select Course Title:</label>
                                                    <div class="col-lg-9">
                                                        <select id="firstSelect" name="course_id" data-placeholder="Select Course Title" class="select">
                                                            <option selected="selected"></option>
                                                            <optgroup label="Select Course Title">
                                                                <?php
                                                                foreach ($allDatas as $oneCourse) {
                                                                    ?>
                                                                    <option value="<?php echo $oneCourse['id']; ?>" <?php if (!empty($_SESSION['EditPost']) && isset($_SESSION['EditPost'])) {
                                                                    echo $_SESSION['EditPost']['course_id'] == $oneCourse['id'] ? 'selected' : '';
                                                                }else {
                                                                    echo $editData['course_id'] == $oneCourse['id'] ? 'selected' : '';
                                                                    }  ?>>
                                                                    <?php echo $oneCourse['title']; ?>
                                                                    </option>
<?php }
?>
                                                            </optgroup>
                                                        </select>
                                                        <p class="text-danger"><?php $obj->sessionMsg('emptyCourse'); ?></p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label">Select Lead Trainer:</label>
                                                    <div class="col-lg-9">
                                                        <select  name="leadTrainer"  data-placeholder="Select Lead Trainer" class="select">
                                                            <option selected="selected"></option>
                                                            <?php
                                                            foreach ($allTrainers as $oneTrainer) {
                                                                if ($oneTrainer['trainer_status'] == "lead_trainer") {
                                                                    ?>
                                                                    <option value="<?php echo $oneTrainer['full_name'] ?>" <?php 
                                                                    if (!empty($_SESSION['EditPost']) && isset($_SESSION['EditPost'])) {
                                                                echo $_SESSION['EditPost']['leadTrainer'] == $oneTrainer['full_name'] ? 'selected' : '';
                                                            }else{
                                                                echo $editData['lead_trainer'] == $oneTrainer['full_name'] ? 'selected' : '';
                                                            } ?>><?php echo $oneTrainer['full_name'] ?> </option>
        <?php
    }
}
?>
                                                        </select>
                                                        <p class="text-danger"><?php $obj->sessionMsg('leadTrainerError'); ?></p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label">Select Lab Assistant:</label>
                                                    <div class="col-lg-9">
                                                        <select  name="labAssist"  data-placeholder="Select Lab Assistant" class="select">
                                                            <option selected="selected"></option>
                                                            <?php
                                                            foreach ($allTrainers as $oneTrainer) {
                                                                if ($oneTrainer['trainer_status'] == "lab_assist") {
                                                                    ?>
                                                                    <option value="<?php echo $oneTrainer['full_name'] ?>" <?php if (!empty($_SESSION['EditPost']) && isset($_SESSION['EditPost'])) {
                                                                echo $_SESSION['EditPost']['labAssist'] == $oneTrainer['full_name'] ? 'selected' : '';
                                                            }else{
                                                                echo $editData['lab_asst'] == $oneTrainer['full_name'] ? 'selected' : '';
                                                            } ?>><?php echo $oneTrainer['full_name'] ?> </option>
        <?php
    }
}
?>
                                                        </select>
                                                        <p class="text-danger"><?php $obj->sessionMsg('labAssistError'); ?></p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-lg-3">Start Date</label>
                                                    <div class="col-lg-9">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-watch2"></i></span>
                                                            <input type="date" class="form-control" placeholder="Start Date"name="start_date" value="<?php 
                                                            if (!empty($_SESSION['EditPost']) && isset($_SESSION['EditPost'])) {
                                                                echo $_SESSION['EditPost']['start_date'];
                                                            }else{
                                                                echo $editData['start_date'];
                                                            } ?>">              
                                                        </div>
                                                        <p class="text-danger"><?php $obj->sessionMsg('start_dateError'); ?></p>
                                                    </div>
                                                </div>



                                                <div class="form-group">
                                                    <label class="control-label col-lg-3">Start Time</label>
                                                    <div class="col-lg-9">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-watch2"></i></span>
                                                            <input type="time" class="form-control" placeholder="Start Time" name="start_time" value="<?php 
                                                            if (!empty($_SESSION['EditPost']) && isset($_SESSION['EditPost'])) {
                                                                echo $_SESSION['EditPost']['start_time'];
                                                            }else{
                                                                echo $editData['start_time'];
                                                            } ?>">             
                                                        </div>
                                                        <p class="text-danger"><?php $obj->sessionMsg('start_timeError'); ?></p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-lg-3">Day</label>
                                                    <div class="col-lg-9">
                                                        <select name="day" class="form-control">
                                                            <option value="day1" <?php if (!empty($_SESSION['EditPost']) && isset($_SESSION['EditPost'])) {
    echo $_SESSION['EditPost']['day'] == 'day1' ? 'selected' : '';
}else{
    echo $editData['day'] == 'day1' ? 'selected' : '';
} ?>>Sat - Mon - Wed</option>
                                                            <option value="day2" <?php if (!empty($_SESSION['EditPost']) && isset($_SESSION['EditPost'])) {
    echo $_SESSION['EditPost']['day'] == 'day2' ? 'selected' : '';
}else{
    echo $editData['day'] == 'day2' ? 'selected' : '';
} ?>>Sun - Tue - Thus</option>
                                                            <option value="day3" <?php if (!empty($_SESSION['EditPost']) && isset($_SESSION['EditPost'])) {
    echo $_SESSION['EditPost']['day'] == 'day3' ? 'selected' : '';
}else{
    echo $editData['day'] == 'day3' ? 'selected' : '';
} ?>>Friday</option>
                                                        </select>
                                                        <p class="text-danger"><?php $obj->sessionMsg('wrongDay'); ?></p>
                                                    </div>
                                                </div>

                                            </fieldset>
                                        </div>

                                        <div class="col-md-6">
                                            <fieldset>
                                                <div class="form-group">
                                                    <label class="control-label col-lg-3">Input Batch Number</label>
                                                    <div class="col-lg-9">
                                                        <input type="text" name = "batch" class="form-control" placeholder="Batch Number" value="<?php 
                                                            if (!empty($_SESSION['EditPost']) && isset($_SESSION['EditPost'])) {
                                                                echo $_SESSION['EditPost']['batch'];
                                                            }else{
                                                                echo $editData['batch_no'];;
                                                            } ?>">
                                                        <p class="text-danger"><?php $obj->sessionMsg('batchNumberError'); ?></p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label">Assistant Trainer:</label>
                                                    <div class="col-lg-9">
                                                        <select id="secondSelect" name="assistTrainer"  data-placeholder="Select Assistant Trainer" class="select">
                                                            <option selected="selected"></option>
<?php
foreach ($allTrainers as $oneTrainer) {
    if ($oneTrainer['trainer_status'] == "assist_trainer") {
        ?>
                                                                    <option class="conditional <?php echo $oneTrainer['courses_id']; ?>" value="<?php echo $oneTrainer['full_name'] ?>" <?php if (!empty($_SESSION['EditPost']) && isset($_SESSION['EditPost'])) {
                                                                echo $_SESSION['EditPost']['assistTrainer'] == $oneTrainer['full_name'] ? 'selected' : '';
                                                            }else{
                                                               echo $editData['asst_trainer'] == $oneTrainer['full_name'] ? 'selected' : ''; 
                                                            } ?>><?php echo $oneTrainer['full_name'] ?> </option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                        <p class="text-danger"><?php $obj->sessionMsg('assistTrainerError'); ?></p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label">Select Lab Number</label>
                                                    <div class="col-lg-9">
                                                        <select data-placeholder="Select Lab Number" class="select" name="lab_num" data-placeholder="Select Lab Number">

                                                            <option selected="selected"></option>
<?php
foreach ($allLabs as $oneLabs) {
    ?>
                                                                <option value="<?php echo $oneLabs['lab_no']; ?>" <?php if (!empty($_SESSION['EditPost']) && isset($_SESSION['EditPost'])) {
        echo $_SESSION['EditPost']['lab_num'] == $oneLabs['lab_no'] ? 'selected' : '';
    }else{
        echo $editData['lab_id'] == $oneLabs['lab_no'] ? 'selected' : '';
    } ?>>Lab <?php echo $oneLabs['lab_no'] ?> </option>
    <?php
}
?>
                                                        </select>
                                                        <p class="text-danger"><?php $obj->sessionMsg('labNumberError'); ?></p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-lg-3">End Date</label>
                                                    <div class="col-lg-9">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-watch2"></i></span>
                                                            <input type="date" class="form-control" placeholder="End Date" name="end_date" value="<?php 
                                                            if (!empty($_SESSION['EditPost']) && isset($_SESSION['EditPost'])) {
                                                                echo $_SESSION['EditPost']['end_date'];
                                                            }else{
                                                                echo $editData['ending_date'];
                                                            } ?>">
                                                        </div>
                                                        <p class="text-danger"><?php $obj->sessionMsg('end_dateError'); ?></p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-lg-3">End Time</label>
                                                    <div class="col-lg-9">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-watch2"></i></span>
                                                            <input type="time" class="form-control" placeholder="End Time" name="end_time" value="<?php 
                                                            if (!empty($_SESSION['EditPost']) && isset($_SESSION['EditPost'])) {
                                                                echo $_SESSION['EditPost']['end_time'];
                                                            }else{
                                                                echo $editData['ending_time'];
                                                            } ?>">                 
                                                        </div>
                                                        <p class="text-danger"><?php $obj->sessionMsg('end_timeError'); ?></p>
                                                    </div>
                                                </div>

                                            </fieldset>
                                        </div>
                                    </div>

                                    <div class="text-right">
                                        <button type="submit" class="btn btn-success">Update Assigned Session <i class="icon-arrow-right14 position-right"></i></button>
                                    </div>

                                </div>
                            </div>
                            <input type="hidden" name = "id" value="<?php echo $editData['id'];?>">
                        </form>
                    </div>
                </div>

                <script>
                    $(function () {
                        var conditionalSelect = $("#secondSelect"),
                                // Save possible options
                                options = conditionalSelect.children(".conditional").clone();

                        $("#firstSelect").change(function () {
                            var value = $(this).val();
                            conditionalSelect.children(".conditional").remove();
                            options.clone().filter("." + value).appendTo(conditionalSelect);
                        }).trigger("change");
                    });
                </script>

<?php
include_once 'footer.php';

?>
