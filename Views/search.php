<?php
include_once '../vendor/autoload.php';

use App\course\Course;

$obj = new Course();
if (isset($_POST['var1'])) {
    $Alldata = $obj->search($_POST);
    // echo "<pre>";
    // print_r($Alldata);
}
?>


<?php include_once 'header.php'; ?>
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Search</span> - Result</h4>
        </div>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Detached sidebar -->
    <!-- Detached sidebar -->
    <?php include_once 'sidebar.php'; ?>
    <!-- /detached sidebar -->
    <!-- /detached sidebar -->


    <!-- Detached content -->
    <div class="container-detached">
        <div class="content-detached">

            <!-- Grid -->
            <div class="row">
                <div class="col-md-12">

                    <!-- display of search results -->
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <h3 class="panel-title">Search results..... </h3>
                        </div>

                        <div class="panel-body">
                            <?php
                            if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                                echo $_SESSION['msg'];
                                unset($_SESSION['msg']);
                            }
                            ?>
                            <table class="footable table table-bordered table-hover mtop" data-page-size="10" data-next-text="Next" data-previous-text="Previous">
                                <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th colspan="3">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
<?php
$serial = 1;
if (isset($Alldata) && !empty($Alldata)) {

    foreach ($Alldata as $Singledata) {
        ?>

                                            <tr>
                                                <td><?php echo $serial++ ?></td>
                                                <td><?php echo $Singledata['title'] ?></td>
                                                <td><?php echo $Singledata['description'] ?></td>
                                                <td><a class="btn btn-default btn-block" href="show.php?id=<?php echo $Singledata['unique_id'] ?>"><i class="icon-make-group position-left"></i> View</a></td>
                                                <td><a class="btn btn-default btn-block" href="edit.php?id=<?php echo $Singledata['unique_id'] ?>"><i class="icon-database-edit2"></i> Edit</a></td>
                                                <td><a class="btn btn-default btn-block" onclick="return checkDelete()" href="delete.php?id=<?php echo $Singledata['unique_id'] ?>"><i class=" icon-reload-alt"></i> Delete</a></td>

                                            </tr>
    <?php
    } 
} else {
    ?>
                                        <tr>
                                            <td colspan="4">
                                                No available data
                                            </td>
                                        </tr>
<?php } ?>
                                </tbody>
                                <tfoot class="hide-if-no-paging">
                                <td colspan="6">
                                    <div class="pagination text-center"></div>
                                </td>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /end of search result -->

                </div>

            </div>
        </div>
        <!-- /grid -->

    </div>
</div>
<!-- /detached content -->


<!-- Footer -->
<?php include_once 'footer.php'; ?>
<!-- script for pagination -->
<script type="text/javascript">

    $(document).ready(function () {
        $('.footable').footable({
//                "paging": {
//                    "enabled": true
//                },
//                "filtering": {
//                    "enabled": true
//                },
//                "sorting": {
//                    "enabled": true
//                }
        });
    });

</script>
</body>
</html>

