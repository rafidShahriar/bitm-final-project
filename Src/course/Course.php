<?php
namespace App\course;
use PDO;

class Course
{
    public $dbuser = 'root';
    public $dbpass = '';
    public $conn = "";
    public $id = "";
    public $uniq_id = "";
    public $course_name = "";
    public $course_description = "";
    public $course_duration = "";
    public $course_type = "";
    public $course_fee = "";
    public $course_offer = "";
    public $data = "";
    public $keywords = "";
    public $errors = "";

    public function __construct()
    {
        if (!isset($_SESSION)) {
            session_start();
        }
        $this->conn = new PDO('mysql:host=localhost;dbname=final_project', $this->dbuser, $this->dbpass);
    }// Construct \\


    public function prepare($data = "")
    {


        if (!empty($data['id'])) {
            $this->id = $data['id'];
        }

        if (!empty($data['title'])) {
            $this->course_name = $data['title'];
        }

        if (!empty($data['duration'])) {
            $this->course_duration = $data['duration'];
        }

        if (!empty($data['course_type'])) {
            $this->course_type = $data['course_type'];
        } 
        

        if (!empty($data['course_fee'])) {
            $this->course_fee = $data['course_fee'];
        } else {
            $this->course_fee = 0;
        }

        if (!empty($data['is_offer'])) {
            $this->course_offer = $data['is_offer'];
        }

        if (!empty($data['description'])) {
            $this->course_description = $data['description'];
        }

        return $this;

    } // prepare \\


    public function sessionMsg($data = "")
    {
        if (!empty($_SESSION["$data"]) && isset($_SESSION["$data"])) {
            echo $_SESSION["$data"];
            unset($_SESSION["$data"]);
        }
    }// sessionMsg \\

    public function validation()
    {
        if (!empty($this->course_name)) {
            $_SESSION['course_name_V'] = $this->course_name;
        } else {
            $_SESSION['course_name_M'] = "Please enter course name *";
            $this->errors = true;
        }

        if (!empty($this->course_duration)) {
            $_SESSION['course_duration_V'] = $this->course_duration;
        }else {
            $_SESSION['course_duration_M'] = "Please select course duration *";
            $this->errors = true;
        }

        if (!empty($this->course_type)) {
            if($this->course_type == '2'){
                if(!empty($this->course_fee) || $this->course_fee!= '0'){
                    $_SESSION['course_fee_M'] = "Can't add course fee * in free course";
                    $this->errors = true;
                }
            }
            elseif($this->course_type == '1') {
                if (!empty($this->course_fee)) {
                    $_SESSION['course_fee_V'] = $this->course_fee;
                } else  {
                    $_SESSION['course_fee_M'] = "Please enter course fee *";
                    $this->errors = true;
                }
            }
            
            $_SESSION['course_type_V'] = $this->course_type;
        } else {
            $_SESSION['course_type_M'] = "Please select course type *";
            $this->errors = true;
        }
    }


    public function store()
    {
          if (!empty($this->errors) == false) {
            try {

                $uniqueId = uniqid();

                $query = "INSERT INTO courses (`id`, `unique_id`, `title`, `duration`, `description`, `course_type`, `course_fee`, `is_offer`, `created`) VALUES (:id, :unique_id, :title, :duration, :description, :course_type, :course_fee, :is_offer, :created)";

                $stmt = $this->conn->prepare($query);
                $stmt->execute(array(
                    ':id' => null,
                    ':unique_id' => $uniqueId,
                    ':title' => $this->course_name,
                    ':duration' => $this->course_duration,
                    ':description' => $this->course_description,
                    ':course_type' => $this->course_type,
                    ':course_fee' => $this->course_fee,
                    ':is_offer' => $this->course_offer,
                    ':created' => date("Y-m-d h:i:s"),

                ));

                $_SESSION['storeSuccess'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>Course name ' . $this->course_name . ' is successfully stored. Thank you.</div>';
                unset($_SESSION['course_duration_V']);
                unset($_SESSION['course_type_V']);
                unset($_SESSION['course_name_V']);
                
                header("location:create.php");
                
            } catch (Exception $exc) {
                echo $exc->getMessage();
            }
        } else {
            header("location:create.php");
        }
    }// store \\


    // show \\
    public function show()
    {
        try {
            $qr = "SELECT * FROM courses WHERE courses.unique_id=" . "'" . $this->id . "'";
            $query = $this->conn->prepare($qr);
            $query->execute();
            $row = $query->fetch(PDO::FETCH_ASSOC);
            $_SESSION['show_data'] = $row;
            return $row;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }// show \\


    public function ShowAllCourse()
    {
        $query = "SELECT * FROM `courses` ";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $this->data[] = $row;
        }
        return $this->data;

    }// ShowAllCourse


    public function update()
    {
        if (!empty($this->errors) == false) {
            try {
                $query = "UPDATE courses SET title = :title, duration = :duration, course_type = :course_type, course_fee = :course_fee,
is_offer = :is_offer, description = :description,  updated= :updated WHERE courses.unique_id = :uid";
                $stmt = $this->conn->prepare($query);
                $stmt->execute(array(
                    ':title' => $this->course_name,
                    ':duration' => $this->course_duration,
                    ':course_type' => $this->course_type,
                    ':course_fee' => $this->course_fee,
                    ':is_offer' => $this->course_offer,
                    ':description' => $this->course_description,
                    ':updated' => date("Y-m-d h:i:s"),
                    ':uid' => $this->id,
                ));

                $_SESSION['updateSuccess'] = '<div class="alert alert-success alert-styled-left"><button
            data-dismiss="alert" class="close"
            type="button"><span>×</span><span class="sr-only">Close</span></button>Course name ' . $this->course_name . ' is
            successfully Updated. Thank you.</div>';

                header("location:edit.php?id=$this->id");
            } catch (PDOException $e) {
                echo 'Error: ' . $e->getMessage();
            }
        } else {
            header("location:edit.php?id=$this->id");
        }
    }

// function for searching \\
    public function search($search = "")
    {
        if (!empty($search['var1'])) {
        $keywords = $search['var1'];
        // print_r($keywords);
        // die();
        $keywords = preg_split('/[\s]+/', $keywords);
          $totalKeywords = count($keywords);
          $query = "SELECT * FROM `courses` WHERE title LIKE ?";

          for($i=1 ; $i < $totalKeywords; $i++){
            $query .= " AND title LIKE ? ";
          }

          $sql=$this->conn->prepare($query);
          foreach($keywords as $key => $keyword){
            $search = '%'.$keyword.'%';
            $sql->bindParam($key+1, $search);
          }
          $sql->execute ();
          if ($sql->rowCount() > 0) {
                while ($result=$sql->fetch(PDO::FETCH_ASSOC)) {
                //echo"<pre>".print_r($result,true)."</pre>";
                    $this->data[] = $result;
            }
            } else {
                $_SESSION['msg'] = 'There was nothing to show. Please search with right keyword.';
            }
            return $this->data;

        } else {
            $_SESSION['msg'] = 'Please insert something.';
        }
        return $this->data;
    }

    // end of search function \\


    public function trash()
    {
        try {

            $query = "UPDATE courses SET is_delete = :is_delete, deleted = :deleted WHERE courses.unique_id = :uid";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':is_delete' => 1,
                ':deleted' => date("Y-m-d h:i:s"),
                ':uid' => $this->id,
            ));

            $_SESSION['deleteSuccess'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>Selected Course is successfully Deleted. Thank you.</div>';
            header('location:index.php');
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }// Trash \\


    public function restore()
    {
        try {

            $query = "UPDATE courses SET is_delete = :is_delete WHERE courses.unique_id = :uid";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':is_delete' => 0,
                ':uid' => $this->id,
            ));

            $_SESSION['deleteSuccess'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>Selected Course is Restored successfully. Thank you.</div>';
            header('location:trashlist.php');
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }// restore \\


    public function delete()
    {

        $unique_id = " '$this->id' ";

        $query = "DELETE FROM `courses` WHERE `courses`.`unique_id` = $unique_id";
        $stmt = $this->conn->prepare($query);
        if ($stmt->execute()) {
            $_SESSION['deleteSuccess'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>Selected Course is Deleted successfully. Thank you.</div>';
            header('location:trashlist.php');
        } // execute
    }// delete \\


}// CLASS Course\\


?>