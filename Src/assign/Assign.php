<?php
namespace App\assign;

use PDO;

class Assign
{
    public $dbuser = 'root';
    public $dbpass = '';
    public $conn = "";
    public $id = "";
    public $data = "";
    public $course_id = '';
    public $batch_no = '';
    public $lead_trainer = '';
    public $asst_trainer = '';
    public $lab_asst = '';
    public $lab_num = '';
    public $start_date = '';
    public $ending_date = '';
    public $start_time = '';
    public $ending_time = '';
    public $day = '';
    public $is_running = '';
    public $assigned_by = '';
    public $created = '';
    public $updated = '';
    public $deleted = '';
    public $errors = '';
    public $course = '';
    public $trainer = '';
    public $lab = '';
    public $editData = '';
    public $Data_for_edit = '';
    public $trashlist = '';


    public function __construct()
    {
        if (!isset($_SESSION)) {
            session_start();
        }
        $this->conn = new PDO('mysql:host=localhost;dbname=final_project', $this->dbuser, $this->dbpass);
    }// Construct \\


    public function prepare($data = "")
    {


        if (!empty($data['id'])) {
            $this->id = $data['id'];
        }

        if (!empty($data['course_id'])) {
            $this->course_id = $data['course_id'];
        }

        if (!empty($data['leadTrainer'])) {
            $this->lead_trainer = $data['leadTrainer'];
        }

        if (!empty($data['assistTrainer'])) {
            $this->asst_trainer = $data['assistTrainer'];
        }

        if (!empty($data['labAssist'])) {
            $this->lab_asst = $data['labAssist'];
        }

        if (!empty($data['batch'])) {
            $this->batch_no = $data['batch'];
        }

        if (!empty($data['lab_num'])) {
            $this->lab_num = $data['lab_num'];
        }

        if (!empty($data['start_date'])) {
            $this->start_date = $data['start_date'];
        }

        if (!empty($data['end_date'])) {
            $this->ending_date = $data['end_date'];
        }

        if (!empty($data['start_time'])) {
            $this->start_time = $data['start_time'];
        }

        if (!empty($data['end_time'])) {
            $this->ending_time = $data['end_time'];
        }

        if (!empty($data['day'])) {
            $this->day = $data['day'];
        }


        return $this;

    } // prepare \\


    public function sessionMsg($data = "")
    {
        if (!empty($_SESSION["$data"]) && isset($_SESSION["$data"])) {
            echo $_SESSION["$data"];
            unset($_SESSION["$data"]);
        }
    }// sessionMsg \\

    


    public function store()
    {
         if ($valid == TRUE) {
                   try {
                           
                       $is_run = 1;
                       $user = 'BITM Admin';
                       
//                        $date = date_create($this->start_time);
//                        echo date_format($date, 'H:i:s'); 
                        // figure out time and date as database friendly formate for further calculation

                        $start_time = date_format(date_create($this->start_time), 'H:i:s');
                        $end_time = date_format(date_create($this->ending_time), 'H:i:s');
                        $start_date = date_format(date_create($this->start_date), 'Y-m-d');
                        $end_date = date_format(date_create($this->ending_date), 'Y-m-d');
                        
        
                $query = "INSERT INTO course_trainer_lab_mapping (`id`, `course_id`, `batch_no`, `lead_trainer`, `asst_trainer`, `lab_asst`, `lab_id`, `start_date`, `ending_date`, `start_time`, `ending_time`, `day`, `is_running`, `assigned_by`, `created`) VALUES (:id, :course_id, :batch_no, :lead_trainer, :asst_trainer, :lab_asst, :lab_id, :start_date, :ending_date, :start_time, :ending_time, :day, :is_running, :assigned_by, :created)";

                $stmt = $this->conn->prepare($query);
                if($stmt->execute(array(
                    ':id' => null,
                    ':course_id' => $this->course_id,
                    ':batch_no' => $this->batch_no,
                    ':lead_trainer' => $this->lead_trainer,
                    ':asst_trainer' => $this->asst_trainer,
                    ':lab_asst' => $this->lab_asst,
                    ':lab_id' => $this->lab_num,
                    ':start_date' => $start_date,
                    ':start_time' => $start_time,
                    ':ending_time' => $end_time,
                    ':ending_date' => $end_date,
                    ':day' => $this->day,
                    ':is_running' => $is_run,
                    ':assigned_by' => $user,
                    ':created' => date("Y-m-d h:i:s"),

                ))){
                     $_SESSION['assignStoreSuccess'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>Course is successfully assigned. Thank you.</div>';
                     
                     session_unset($_SESSION['POST']);
                     header("location:index.php");
                }
               
                

            } catch (Exception $exc) {
                echo $exc->getMessage();
            }
            } else {
            $_SESSION['DataError'] = '<h4 class= "text-info">Sorry data not stored to Database, please try again</h4>';
            header('location:index.php');
            }

}// store \\
//SELECT * FROM courses LEFT JOIN course_trainer_lab_mapping on courses.id = course_trainer_lab_mapping.course_id LEFT JOIN labinfo on course_trainer_lab_mapping.course_id = labinfo.course_id

    // show \\
    public function show()
    {
        try {
            $qr = "SELECT * FROM courses  LEFT JOIN course_trainer_lab_mapping ON course_trainer_lab_mapping.course_id = courses.id  WHERE course_trainer_lab_mapping.id=" . "'" . $this->id . "'";
            $query = $this->conn->prepare($qr);
            $query->execute();
            $row = $query->fetch(PDO::FETCH_ASSOC);
            $_SESSION['show_data'] = $row;
            return $row;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }// show \\
    // show \\
    public function searchShow()
    {
        try {
            $qr = "SELECT * FROM courses LEFT JOIN course_trainer_lab_mapping on courses.id = course_trainer_lab_mapping.course_id LEFT JOIN labinfo on course_trainer_lab_mapping.course_id = labinfo.course_id WHERE courses.unique_id=" . "'" . $this->id . "'";
            $query = $this->conn->prepare($qr);
            $query->execute();
            $row = $query->fetch(PDO::FETCH_ASSOC);
            $_SESSION['show_data'] = $row;
            return $row;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }// show \\

         public function assignShow()
     {
         try {
             $qr = "SELECT * FROM `course_trainer_lab_mapping` WHERE id =" . "'" . $this->id . "'";
             $query = $this->conn->prepare($qr);
             $query->execute();
             $row = $query->fetch(PDO::FETCH_ASSOC);
             $_SESSION['show_data'] = $row;
             return $row;
         } catch (PDOException $e) {
             echo 'Error: ' . $e->getMessage();
         }
     }// show \\
 
 
     public function allShow()
     {
         try {
             $qr = "SELECT * FROM courses LEFT JOIN course_trainer_lab_mapping on courses.id = course_trainer_lab_mapping.course_id LEFT JOIN labinfo on course_trainer_lab_mapping.course_id = labinfo.course_id  LEFT JOIN trainers on labinfo.course_id = trainers.courses_id WHERE courses.id =" . "'" . $this->id . "'";
             $query = $this->conn->prepare($qr);
             $query->execute();
             $row = $query->fetch(PDO::FETCH_ASSOC);
             $_SESSION['show_data'] = $row;
             return $row;
         } catch (PDOException $e) {
             echo 'Error: ' . $e->getMessage();
         }
    }// show \\


    public function allData()
    {
        try {
            $query = "SELECT DISTINCT `title`, `id` FROM courses";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $this->data[] = $row;
            }
            return $this->data;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }// ShowAllCourse
    
    
//        public function allData()
//    {
//        try {
//            $query = "SELECT * FROM courses LEFT JOIN trainers ON courses.id = trainers.courses_id LEFT JOIN labinfo on courses.id = labinfo.course_id";
//            $stmt = $this->conn->prepare($query);
//            $stmt->execute();
//
//            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
//                $this->data[] = $row;
//            }
//            return $this->data;
//        } catch (PDOException $e) {
//            echo 'Error: ' . $e->getMessage();
//        }
//    }// ShowAllCourse
//    

    public function ShowAllCourse()
    {
        try {
            $query = "SELECT * FROM courses";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $this->data[] = $row;
            }
            return $this->data;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }// ShowAllCourse
    


    public function ShowAllAssign()
    {
        try {
            $query = "SELECT * FROM courses LEFT JOIN course_trainer_lab_mapping on course_trainer_lab_mapping.course_id = courses.id ";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $this->editData[] = $row;
            }
            return $this->editData;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }// ShowAllAssign
    
    public function CourseTitle()
    {
        try {
            $query = "SELECT `id`,`title` FROM `courses`";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $this->course[] = $row;
            }
            return $this->course;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }// ShowAllCourse
    


    public function AllTrainers()
    {
        $query = "SELECT DISTINCT `courses_id`,`trainer_status`, `team`,`full_name` FROM `trainers`";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $this->trainer[] = $row;
        }
        return $this->trainer;

    }// AllTrainers


    public function AllLabs()
    {
        $query = "SELECT DISTINCT `lab_no` FROM labinfo";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $this->lab[] = $row;
        }
        return $this->lab;

    }// AllLabs
    
//UPDATE courses SET title = :title, duration = :duration, course_type = :course_type, course_fee = :course_fee, is_offer = :is_offer, description = :description,  updated= :updated WHERE courses.unique_id = :uid";

    public function update(){
        try{
            
            $start_time = date_format(date_create($this->start_time), 'H:i:s');
            $end_time = date_format(date_create($this->ending_time), 'H:i:s');
            $start_date = date_format(date_create($this->start_date), 'Y-m-d');
            $end_date = date_format(date_create($this->ending_date), 'Y-m-d');
            
            $query = "UPDATE course_trainer_lab_mapping SET course_id = :course_id, batch_no = :batch_no, lead_trainer = :lead_trainer, asst_trainer = :asst_trainer, lab_asst = :lab_asst, lab_id = :lab_id, start_date = :start_date, ending_date = :ending_date, start_time = :start_time, ending_time = :ending_time, day = :day, updated= :updated WHERE course_trainer_lab_mapping.id = :id";

                $stmt = $this->conn->prepare($query);
                
                if($stmt->execute(array(
                    ':course_id' => $this->course_id,
                    ':batch_no' => $this->batch_no,
                    ':lead_trainer' => $this->lead_trainer,
                    ':asst_trainer' => $this->asst_trainer,
                    ':lab_asst' => $this->lab_asst,
                    ':lab_id' => $this->lab_num,
                    ':start_date' => $start_date,
                    ':start_time' => $start_time,
                    ':ending_time' => $end_time,
                    ':ending_date' => $end_date,
                    ':day' => $this->day,
                    ':updated' => date("Y-m-d h:i:s"),
                    ':id' => $this->id,
                ))){
                     $_SESSION['assignUpdateSuccess'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>Course is successfully Updated. Thank you.</div>';
                    
//                    session_unset($_SESSION['EditPost']);
                    header("location:edit.php?id=$this->id");
                }
            
        } catch (Exception $ex) {
             header("location:edit.php?id=$this->id");
        }
    }

// function for searching \\
    public function search($search = "")
     {  //print_r($search);
        //die();
        if ($search['search_key'] == 'course' && !empty($search['var1'])) {
            $keywords = $search['var1'];
        //print_r($keywords);
        //die();
        $keywords = preg_split('/[\s]+/', $keywords);
          $totalKeywords = count($keywords);
          $query = "SELECT * FROM courses LEFT JOIN course_trainer_lab_mapping on courses.id = course_trainer_lab_mapping.course_id LEFT JOIN labinfo on course_trainer_lab_mapping.course_id = labinfo.course_id WHERE courses.title LIKE ?";

          for($i=1 ; $i < $totalKeywords; $i++){
            $query .= " AND title LIKE ? ";
          }

          $sql=$this->conn->prepare($query);
          foreach($keywords as $key => $keyword){
            $search = '%'.$keyword.'%';
            $sql->bindParam($key+1, $search);
          }
          $sql->execute ();
          if ($sql->rowCount() > 0) {
                while ($result=$sql->fetch(PDO::FETCH_ASSOC)) {
                //echo"<pre>".print_r($result,true)."</pre>";
                    $this->data[] = $result;
            }
            } else {
                $_SESSION['msg'] = 'There was nothing to show. Please search with right keyword.';
            }
            //return $this->data;
        } 
        elseif ($search['search_key'] == 'lab' && !empty($search['var1'])) {
            $keywords = $search['var1'];
        //print_r($keywords);
        //die();
        $keywords = preg_split('/[\s]+/', $keywords);
          $totalKeywords = count($keywords);
          $query = "SELECT * FROM labinfo LEFT JOIN courses on labinfo.course_id = courses.id LEFT JOIN course_trainer_lab_mapping on course_trainer_lab_mapping.lab_id = labinfo.id WHERE labinfo.lab_no LIKE ?";

          for($i=1 ; $i < $totalKeywords; $i++){
            $query .= " AND lab_no LIKE ? ";
          }

          $sql=$this->conn->prepare($query);
          foreach($keywords as $key => $keyword){
            $search = '%'.$keyword.'%';
            $sql->bindParam($key+1, $search);
          }
          $sql->execute ();
          if ($sql->rowCount() > 0) {
                while ($result=$sql->fetch(PDO::FETCH_ASSOC)) {
                //echo"<pre>".print_r($result,true)."</pre>";
                    $this->data[] = $result;
            }
            } else {
                $_SESSION['msg'] = 'There was nothing to show. Please search with right keyword.';
            }
        }
        else {
            echo "Please search with right keyword";
        }
        return $this->data;
        
    }

    // end of search function \\


    public function trash()
    {
        try {
                        
            $query = "UPDATE `course_trainer_lab_mapping` SET is_running = :is_running, deleted = :deleted WHERE course_trainer_lab_mapping.id = :rid";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':is_running' => 0,
                ':deleted' => date("Y-m-d h:i:s"),
                ':rid' => $this->id,
            ));

            $_SESSION['deleteSuccess'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>Selected Course is successfully Disabled. Thank you.</div>';
            header('location:list.php');
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }// Trash \\



    public function restore()
    {
        try {

            $query = "UPDATE `course_trainer_lab_mapping` SET is_running = :is_running WHERE course_trainer_lab_mapping.id = :rid";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':is_running' => 1,
                ':rid' => $this->id,
            ));

            $_SESSION['deleteSuccess'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>Selected Course is Restored successfully. Thank you.</div>';
            header('location:trashlist.php');
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }// restore \\


    public function delete()
    {

        $unique_id = " '$this->id' ";

        $query = "DELETE FROM `course_trainer_lab_mapping` WHERE `course_trainer_lab_mapping`.`id` = $this->id";
        $stmt = $this->conn->prepare($query);
        if ($stmt->execute()) {
            $_SESSION['deleteSuccess'] = '<div class="alert alert-success alert-styled-left"><button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>Selected Course is Deleted successfully. Thank you.</div>';
            header('location:trashlist.php');
        } // execute
    }// delete \\
    
    
    
    
    public function trashlist()
    {
        try {
            $qr = "SELECT * FROM `course_trainer_lab_mapping` WHERE `is_running`=0 ";
            $query = $this->conn->prepare($qr);
            $query->execute();
            
            while($row = $query->fetch(PDO::FETCH_ASSOC)){
                $this->trashlist[] = $row;
            }
            return $this->trashlist;
            
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }// show \\
    
    
    


}// CLASS Course\\


?>